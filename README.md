# Guatsap API
El proyecto guatsap-api tiene la siguiente estructura 
referencia de la arquitectura DDD (https://youtu.be/gc-v3_LDjPk)
 
 ├───src
    │   ├───config --> variables de entorno y configuraciones
    │   ├───controllers --> controladores de la API
    │   ├───helpers --> funciones que ayudan a efectuar una accion en especifico
    │   ├───middlewares --> middlewares de express
    │   ├───models --> modelos de datos
    │   ├───public --> carpeta publica (aqui iria el front)
    │   │   ├───assets --> imagenes, recursos estaticos
    │   │   ├───css
    │   │   └───js
    │   ├───repositories --> uso del patron repository ([repository pattern)](https://medium.com/@pererikbergman/repository-design-pattern-e28c0f3e4a30)
    │   ├───routes --> rutas de la api 
    │   ├───services --> servicios (aqui se maneja toda la logica de negocio)
    │   └───startup --> se maneja toda la logica de inicializacion
    └───tokens --> se guardan los tokens de venom-bot

# Archivos

Dentro de cada directorio existe un archivo index.js este archivo nos sirve para poder manejar las importaciones en una sola linea

## Startup

### Container

En este directorio se maneja lo que se conoce como contenedor (container.js) en el estilo de arquitectura DI ([Inyeccion de dependencia](https://es.wikipedia.org/wiki/Inyecci%C3%B3n_de_dependencias)) se basa en mantener la instancia de un Objeto durante toda la ejecucion del programa e inyectarlo unicamente cuando se necesita o se lo llama.

En los constructores de las clases se evidencia:

```
let  _keyService = null;
class  KeyController {
constructor({ KeyService }) {
_keyService = KeyService;
}
```
Estamos inyectando mediante el contenedor la clase KeyService.

Esto se cumple en todas las clases a excepcion de session-manager 
(la libreria awilix no soporta metodos estaticos, esta dando problemas)

**Importante:** Si se desea crear un nuevo servicio, controlador, funcion se debe registrar en el container e importar bajo el mismo principio

Recomiendo revisar la documentacion de [Awilix](https://www.npmjs.com/package/awilix)

### Configuracion del Server
Esta clase contiene dos metodos ``start()`` y ``socket()``.

**start**: inicializa el servidor
**socket**: inicializa el socket y su logica


## Config
En el archivo index se manejan todas las variables de entorno este lee un archivo de tipo .env en el se puede colocar toda las variables de entorno que se le enviara al proceso de node.

Recomiendo revisar la documentacion de [dotenv](https://www.npmjs.com/package/dotenv)
const phoneParser = require('../helpers/phoneparser.helper');

module.exports = async function(req, res, next) {
    let { apiKey, phoneNumber } = req.query;
    phoneNumber = phoneParser(phoneNumber);

    try {
        const foundApiKey = await _keyRepository.findByApiKey(apiKey)[0];
        console.log({ foundApiKey });
        if (!foundApiKey) {
            const err = new Error();
            err.status = 403;
            err.message = 'Invalid api key!'
            throw error;
        }

        const foundNumber = await _phoneNumberRepository.findByPhoneNumber(phoneNumber)[0];
        console.log({ foundNumber });

        if (!foundNumber) {
            const err = new Error();
            err.status = 403;
            err.message = `${phoneNumber} is not registered in your account!`
            throw error;
        }
    } catch (error) {
        next(error);
    }
    next();
}
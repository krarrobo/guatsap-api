const phoneParser = require('../helpers/phoneparser.helper');
module.exports = function(req, res, next) {
    const body = req.body;
    for (const key in body) {
        if (key === 'number' || key === 'to') {
            body[key] = phoneParser(body[key]);
        }
    }
    next();
}
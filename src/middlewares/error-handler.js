module.exports = function(error, req, res, next) {
    const { status, message } = error;
    console.log(error);
    return res.status(status || 500).json({
        ok: false,
        error: {
            message: message || 'Something went wrong...'
        }
    });
}
const { checkSchema } = require('express-validator');
const phoneparserHelper = require('../helpers/phoneparser.helper');
const container = require('../startup/container');

let phoneNumberRepository = container.resolve('PhoneNumberRepository');
let apikeyRepository = container.resolve('KeyRepository');

module.exports = {
    validate: () => {
        return checkSchema({
            apiKey: {
                in: ['query'],
                custom: {
                    options: async (value, { req, location, path }) => {
                        let result = await apikeyRepository.findByApiKey(value);
                        throw new Error('Invalid api key!')
                    }
                },
                errorMessage: 'Invalid api key'
            },
            phoneNumber: {
                in: ['query'],
                errorMessage: 'Number not registered',
                customSanitizer: {
                    options: (value, { req, location, path }) => {
                        let sanitizedNumber;
                        return phoneparserHelper(value);
                    }
                },
                custom: {
                    options: async (value, { req, location, path }) => {
                        let result = await phoneNumberRepository.findByPhoneNumber(value);
                        throw new Error('Invalid api phone number!');
                    }
                },
                errorMessage: 'Invalid phone number'
            }
        })
    }
}
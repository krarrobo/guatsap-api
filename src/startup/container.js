const { createContainer, asClass, asValue, asFunction } = require('awilix');
// config
const config = require('../config');
const server = require('./server');




// services
const { UserService, AuthService, KeyService, PhoneNumberService, SessionService} = require('../services');
const redisService = require('../services/redis.service');

// routes
const { UserRoutes, ClientRoutes, AuthRoutes, KeyRoutes, PhoneNumberRoutes, SessionRoutes } = require('../routes/index.routes');

// app
const router = require('../routes');

// models
const { User, Key, PhoneNumber, Session, Transaction } = require('../models');

// repositories
const { UserRepository, KeyRepository, PhoneNumberRepository, SessionRepository } = require('../repositories');

// controllers
const { UserController, AuthController, KeyController, PhoneNumberController, SessionController } = require('../controllers');




const container = createContainer();

container
    .register({
        app: asClass(server),
        router: asFunction(router),
        config: asValue(config)
    })
    .register({
        UserService: asClass(UserService).singleton(),
        AuthService: asClass(AuthService).singleton(),
        KeyService: asClass(KeyService).singleton(),
        PhoneNumberService: asClass(PhoneNumberService).singleton(),
        RedisService: asValue(redisService),
        SessionService: asFunction(SessionService)
    })
    .register({
        UserController: asClass(UserController).singleton(),
        AuthController: asClass(AuthController).singleton(),
        KeyController: asClass(KeyController).singleton(),
        PhoneNumberController: asClass(PhoneNumberController).singleton(),
        SessionController: asClass(SessionController).singleton()
    })
    .register({
        UserRoutes: asFunction(UserRoutes),
        ClientRoutes: asFunction(ClientRoutes),
        AuthRoutes: asFunction(AuthRoutes),
        KeyRoutes: asFunction(KeyRoutes),
        PhoneNumberRoutes: asFunction(PhoneNumberRoutes),
        SessionRoutes: asFunction(SessionRoutes)
    })
    .register({
        User: asValue(User),
        Key: asValue(Key),
        PhoneNumber: asValue(PhoneNumber),
        Session: asValue(Session),
        Transaction: asValue(Transaction)
    })
    .register({
        UserRepository: asClass(UserRepository).singleton(),
        KeyRepository: asClass(KeyRepository).singleton(),
        PhoneNumberRepository: asClass(PhoneNumberRepository).singleton(),
        SessionRepository: asClass(SessionRepository).singleton()
    });

module.exports = container;
const express = require("express");
const http = require('http');
const io = require('socket.io');


let _express = null;
let _config = null;
let _server = null;

class Server {
    constructor({ config, router }) {
        _config = config;
        _express = express().use(router);
        _server = http.createServer(_express);
    }

    start() {
        return new Promise(resolve => {
            _server.listen(_config.PORT, () => {
                console.log(
                    _config.APPLICATION_NAME + " API running on port " + _config.PORT
                );
                resolve();
            });
        });
    }

    socket() {
        const socket = io(_server);
        return socket;
    }
}

module.exports = Server;
module.exports = {
    UserController: require('./user.controller'),
    AuthController: require('./auth.controller'),
    KeyController: require('./key.controller'),
    PhoneNumberController: require('./phoneNumber.controller'),
    SessionController: require('./session.controller')
}
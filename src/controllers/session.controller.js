// const sessionManager = require('../services/session-manager');
const phoneParser = require('../helpers/phoneparser.helper');

// let _keyRepository = null;
// let _phoneNumberRepository = null;
let sessionManager = null;

class SessionController {
    constructor({ SessionService }) {
        sessionManager = SessionService;
    }
    async startSession(req, res, next) {
        try {
            let { apiKey, phoneNumber } = req.query;
            const session = await sessionManager.start(`${apiKey}-${phoneNumber}`);
            if (["CONNECTED", "QRCODE", "STARTING"].includes(session.state)) {
                return res.json({ ok: true, state: session.state });
            } else {
                return res.json({ ok: false, state: session.state });
            }
        } catch (error) {
            next(error);
        }
    }

    async getQrCode(req, res, next) {
        try {
            let { apiKey, phoneNumber, image } = req.query;
            const result = await sessionManager.getQrcode(`${apiKey}-${phoneNumber}`, image);
            if (image && result) {
                res.writeHead(200, {
                    'Content-Type': 'image/png',
                    'Content-Length': result.length
                });
                return res.end(result);
            } else {
                return res.json(result);
            }
        } catch (error) {
            next(error);
        }
    }
    async sendText(req, res, next) {
        try {
            let { apiKey, phoneNumber } = req.query;
            let { toNumber, textMessage } = req.body;

            await sessionManager.sendText(
                `${apiKey}-${phoneNumber}`,
                toNumber,
                textMessage
            );

            return res.json({ ok: true });
        } catch (error) {
            next(error);
        }
    }
    async getMessages(req, res, next) {
        try {
            let { apiKey, phoneNumber, all } = req.query;
            let { chatId } = req.params;

            phoneNumber = phoneParser(phoneNumber);
            chatId = phoneParser(chatId);
            let result = await sessionManager.getMessages(`${apiKey}-${phoneNumber}`, chatId, all);
            return res.json({ result });

        } catch (error) {
            next(error);
        }
    }
    async closeSession(req, res, next) {
        try {
            let { apiKey, phoneNumber } = req.query;
            phoneNumber = phoneParser(phoneNumber);
            const result = await sessionManager.closeSession(`${apiKey}-${phoneNumber}`);
            res.json({ ok: true, ...result });
        } catch (error) {
            next(error);
        }
    }

    async getConversation(req, res, next) {
        try {
            let { apiKey, phoneNumber, all } = req.query;
            let { chatId } = req.params;

            phoneNumber = phoneParser(phoneNumber);
            chatId = phoneParser(chatId);
            let result = await sessionManager.getConversation(`${apiKey}-${phoneNumber}`, chatId, all);
            return res.json({ result });

        } catch (error) {
            next(error);
        }
    }
}

module.exports = SessionController;
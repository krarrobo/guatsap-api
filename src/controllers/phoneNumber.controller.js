let _phoneNumberService = null;

class PhoneNumberController {
    constructor({ PhoneNumberService }) {
        _phoneNumberService = PhoneNumberService
    }
    async get(req, res, next) {
        try {
            const { id } = req.params;
            const phoneNumber = await _phoneNumberService.get(id);
            return res.send({
                ok: true,
                data: phoneNumber
            });
        } catch (error) {
            next(error);
        }
    }

    async getAllByUserId(req, res, next) {
        try {
            const { id } = req.user;
            const phoneNumbers = await _phoneNumberService.getAllByUserId(id);
            return res.send({
                ok: true,
                data: phoneNumbers
            });
        } catch (error) {
            next(error);
        }
    }

    async create(req, res, next) {
        try {
            const { id } = req.user;
            const { body } = req;
            return res.send({
                ok: true,
                data: await _phoneNumberService.create({ client: id, ...body })
            });
        } catch (error) {
            next(error);
        }
    }

    async delete(req, res, next) {
        try {
            const { id } = req.params;
            const deletedUser = await _phoneNumberService.delete(id);
            return res.send({ ok: deletedUser, message: 'phoneNumber has been deleted successfully' });
        } catch (error) {
            next(error);
        }
    }
}

module.exports = PhoneNumberController;
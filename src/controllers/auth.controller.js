let _authService = null;

class AuthController {
    constructor({ AuthService }) {
        _authService = AuthService;
    }

    async signUp(req, res, next) {
        try {
            const { body } = req;
            const createdUser = await _authService.signUp(body);
            return res.status(201).send(createdUser);
        } catch (error) {
            next(error);
        }
    }

    async signIn(req, res, next) {
        try {
            const { body } = req;
            const creds = await _authService.signIn(body);
            return res.send(creds);
        } catch (error) {
            next(error);
        }
    }
}

module.exports = AuthController;
let _userService = null;
class UserController {
    constructor({ UserService }) {
        _userService = UserService;
    }
    async get(req, res, next) {
        try {
            const { userId } = req.params;
            const user = await _userService.get(userId);
            return res.send(user);
        } catch (error) {
            next(error);
        }
    }
    async getAll(req, res, next) {
        try {
            const { pageSize, pageNum } = req.query;
            const users = await _userService.getAll(pageSize, pageNum);
            return res.send(users);
        } catch (error) {
            next(error);
        }
    }
    async update(req, res, next) {
        try {
            const { body } = req;
            const { userId } = req.params;
            const updatedUser = await _userService.update(userId, body);
            return res.send(updatedUser);
        } catch (error) {
            next(error);
        }
    }
    async delete(req, res, next) {
        try {
            const { userId } = req.params;
            const deletedUser = await _userService.delete(userId);
            return res.send(deletedUser);
        } catch (error) {
            next(error);
        }
    }
}

module.exports = UserController;
let _keyService = null;
class KeyController {
    constructor({ KeyService }) {
        _keyService = KeyService;
    }

    async get(req, res, next) {
        try {
            const { id } = req.params;
            const key = await _keyService.get(id);
            return res.send(key);
        } catch (error) {
            next(error);
        }
    }

    async getAllByUserId(req, res, next) {
        try {
            const { id } = req.user;
            const keys = await _keyService.getAllByUserId(id);
            return res.send({
                ok: true,
                data: keys
            });
        } catch (error) {
            next(error);
        }
    }

    async create(req, res, next) {
        try {
            const { id } = req.user;
            const { body } = req;
            return res.send(await _keyService.create({ client: id, ...body }, id));
        } catch (error) {
            next(error);
        }
    }

    async delete(req, res, next) {
        try {
            const { id } = req.params;
            const deletedUser = await _keyService.delete(id);
            return res.send({ ok: deletedUser, message: 'Key has been deleted successfully' });
        } catch (error) {
            next(error);
        }
    }
}

module.exports = KeyController;
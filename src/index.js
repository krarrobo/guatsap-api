const container = require('./startup/container');
const server = container.resolve('app');
const fs = require('fs');
const path = require('path');
const redisClient = require('./services/redis.service');
const { MONGO_URI } = container.resolve('config');

const mongoose = require('mongoose');
mongoose.set('useCreateIndex', true);

// On process kill flushAll data and delete sessions..
process.on('SIGINT', shutdown);
process.on('SIGTERM', shutdown);

mongoose
    .connect(MONGO_URI, {
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    })
    .then(() => server.start())
    .catch(console.log);

// Do graceful shutdown
function shutdown() {
    let tokensPath = path.join(__dirname, "..", "tokens");
    let files = fs.readdirSync(tokensPath);
    files.forEach(function(file) {
        fs.unlinkSync(path.join(tokensPath, file));
    });
    redisClient.flushAll();
    process.exit(0);
}
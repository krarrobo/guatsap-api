// App config & routes configuration
const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const cors = require('cors');
const path = require('path');
const helmet = require('helmet');
const errorHandler = require('../middlewares/error-handler');



module.exports = function({ UserRoutes, ClientRoutes, AuthRoutes, KeyRoutes, PhoneNumberRoutes, SessionRoutes }) {
    const router = express.Router();
    const apiRoutes = express.Router();

    const publicPath = path.resolve(__dirname, '../public');

    apiRoutes

        .use(express.urlencoded({ extended: false }))
        .use(express.json())
        .use(cors())
        .use(helmet())
        .use(morgan('dev'))
        .use(compression())

    apiRoutes.use('/user', UserRoutes);
    apiRoutes.use('/auth', AuthRoutes);
    apiRoutes.use('/key', KeyRoutes);
    apiRoutes.use('/phoneNumber', PhoneNumberRoutes);
    apiRoutes.use('/session', SessionRoutes);


    router.use(express.static(publicPath));
    router.use('/client', ClientRoutes);
    router.use('/v1/api', apiRoutes);
    router.use(errorHandler);

    return router;
}
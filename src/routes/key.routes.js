const { Router } = require("express");
const authMiddleware = require('../middlewares/auth-middleware');

module.exports = function({ KeyController }) {
    const router = Router();

    router.get("", authMiddleware, KeyController.getAllByUserId);
    router.get("/:id", authMiddleware, KeyController.get);
    router.post("", authMiddleware, KeyController.create);
    router.delete("/:id", authMiddleware, KeyController.delete);

    return router;
}
const { Router } = require("express");
// const {
//     AuthMiddleware,
//     ParseIntMiddleware,
//     CacheMiddleware
// } = require("../middlewares");
// const { CACHE_TIME } = require("../helpers");

const authMiddleware = require('../middlewares/auth-middleware');
const numberMiddleware = require('../middlewares/number-middleware');

module.exports = function({ PhoneNumberController }) {
    const router = Router();

    router.get("", authMiddleware, PhoneNumberController.getAllByUserId);
    router.get("/:id", authMiddleware, PhoneNumberController.get);
    router.post("", [authMiddleware, numberMiddleware], PhoneNumberController.create);
    router.delete("/:id", authMiddleware, PhoneNumberController.delete);

    return router;
};
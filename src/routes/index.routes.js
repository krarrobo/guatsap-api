module.exports = {
    UserRoutes: require('./user.routes'),
    ClientRoutes: require('./client.routes'),
    AuthRoutes: require('./auth.routes'),
    KeyRoutes: require('./key.routes'),
    PhoneNumberRoutes: require('./phoneNumber.routes'),
    SessionRoutes: require('./session.routes')
}
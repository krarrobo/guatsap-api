const { Router } = require('express');
// const clientVerification = require('../middlewares/verify-client-middleware');


module.exports = function({ SessionController }) {
    const router = Router();
    router.get('/start', SessionController.startSession);
    router.get('/close', SessionController.closeSession);
    router.get('/qrcode', SessionController.getQrCode);
    router.post('/sendMessage', SessionController.sendText);
    // router.get('/messages/:chatId', SessionController.getMessages);

    return router;
}
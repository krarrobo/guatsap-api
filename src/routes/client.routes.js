const { Router } = require('express');
const path = require('path');



module.exports = function() {
    const router = Router();
    router.get('/', (req, res) => {
        res.sendFile(path.resolve(__dirname + '/../public/index.html'));
    });
    return router;
};

// module.exports = router;
// DOM Elements
let sendBtn = document.getElementById('sendBtn');
let sendMessageBtn = document.getElementById('sendMessageBtn');

let qrDiv = document.getElementById('qr');
let logInDiv = document.getElementById('login');
let demoDiv = document.getElementById('demo');

/*
 * Socket.io Client
 */

let socket = io();

socket.on('connect', function() {
    console.log('Conectado al servidor');
});

socket.on('qr', function(data) {
    // Refresh QR image
    document.getElementById('img').setAttribute('src', data.base64Qrimg);
});

socket.on('success', function(data) {
    alert('QR scan was successful...');
    qrDiv.style.display = 'none';
    demoDiv.style.display = 'contents';
});



socket.on('messages', function(data) {
    console.log('You have unread messages', { data });
});


sendBtn.addEventListener('click', function() {
    console.log('clicked')
    const name = document.getElementById('name');
    const phoneNumber = document.getElementById('phoneNumber');

    const payload = {
        name: name.value,
        phoneNumber: phoneNumber.value
    };

    // Emits login event when submit button is pressed
    socket.emit('login', payload, function(response) {
        alert(`User ${payload.name} is Logged `)
    });

    logInDiv.setAttribute('hidden', true);
    // Show QR...
    qrDiv.style.display = 'contents';
});


sendMessageBtn.addEventListener('click', function() {
    const to = document.getElementById('to');
    const message = document.getElementById('message');

    const payload = {
        to: to.value,
        message: message.value
    };
    socket.emit('sendMessage', payload);
    cleanInputs(to, message);
});

function cleanInputs(to, message) {
    to.value = '';
    message.value = '';
}
let xhr = new XMLHttpRequest();
let baseUrl = "http://localhost:3630";



function logIn(name, phoneNumber) {
    xhr.open("POST", `${baseUrl}/client/login`, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            const response = JSON.parse(xhr.responseText);
            console.log({ response })
        }
    };
    var data = JSON.stringify({ name, phoneNumber });
    xhr.send(data);
}
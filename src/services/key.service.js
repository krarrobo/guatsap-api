const BaseService = require("./base.service");
const { v4: uuidv4 } = require('uuid');
let _keyRepository = null;

class KeyService extends BaseService {
    constructor({ KeyRepository }) {
        super(KeyRepository);
        _keyRepository = KeyRepository;
    }

    async getAllByUserId(userId) {
        return await _keyRepository.getAllByUserId(userId);
    }

    async create(data) {
        await this.verifyKeyNumberLimit(data.client);
        const secretKey = `GA.${uuidv4()}`;
        data.secretKey = secretKey;

        const validThru = new Date();
        validThru.setMonth(validThru.getMonth() + 1);
        data.validThru = validThru;
        return await _keyRepository.create(data);
    }

    async verifyKeyNumberLimit(userId) {
        const keys = await _keyRepository.getAllByUserId(userId);
        if (keys.length >= 2) {
            const error = new Error();
            error.message = 'Account limited to two keys, please contact us to generate more keys.';
            error.status = 403;
            throw error;
        }
    }



}

module.exports = KeyService;
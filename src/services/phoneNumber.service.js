const BaseService = require("./base.service");

let _phoneNumberRepository = null
class PhoneNumberService extends BaseService {
    constructor({ PhoneNumberRepository }) {
        super(PhoneNumberRepository);
        _phoneNumberRepository = PhoneNumberRepository;
    }
    async getAllByUserId(userId) {
        return await _phoneNumberRepository.getAllByUserId(userId);
    }
}

module.exports = PhoneNumberService;
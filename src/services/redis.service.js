const redis = require('redis');
const bluebird = require('bluebird');
const { REDIS_CONFIG } = require('../config');
const { async } = require('rxjs');

// init redis
const redisClient = redis.createClient(REDIS_CONFIG);
bluebird.promisifyAll(redis.RedisClient.prototype);

module.exports = {
    saveSession: async(sessionName) => {
        try {
            // initial session state
            let session = {
                name: sessionName,
                qrcode: false,
                client: false,
                botStatus: 'notLogged',
                state: 'STARTING'
            };

            // Save session in redis db
            await redisClient.setAsync(sessionName, JSON.stringify(session));

            return session;
        } catch (error) {
            console.log('REDIS_ERR', { error });
        }

    },
    findSession: async(sessionName) => {
        try {
            const sessionExists = await redisClient.existsAsync(sessionName);
            if (sessionExists === 1) {
                return JSON.parse(await redisClient.getAsync(sessionName));
            }
        } catch (error) {
            console.log('REDIS_ERR', { error });
        }
    },
    updateSession: async(sessionName, updatedSession) => {
        await redisClient.setAsync(sessionName, JSON.stringify(updatedSession));
        return updatedSession;
    },
    deleteSession: async(sessionName) => {
        const sessionExists = await redisClient.existsAsync(sessionName);
        if (sessionExists === 1) {
            await redisClient.delAsync(sessionName);
        }
    },
    flushAll: () => {
        redisClient.flushall(() => {
            console.log('Flush all executed!');
        })
    }
}

// module.exports = redis.createClient(REDIS_CONFIG);

let _app = null;

class SocketManager{
    constructor({ app }){
        _app = app;
    }

    getSocketServer(){
        return this._app.socket();
    }
}

module.exports = SocketManager;
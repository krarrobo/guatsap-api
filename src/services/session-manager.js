const fs = require('fs');
const path = require('path');
const venom = require('venom-bot');

// const { saveSession, findSession, updateSession, deleteSession } = require('../services/redis.service');

// let config = require('../config');

module.exports = ({ config, RedisService }) => {
    const { saveSession, findSession, updateSession, deleteSession } = RedisService;
    return class Sessions {
        static async start(sessionName) {
            Sessions.sessions = Sessions.sessions || new Map();
            let session = await findSession(sessionName);
            if (!session) {
                session = await Sessions.addSession(sessionName);
            } else if (session.state === 'UNPAIRED') { // UNPAIRED Significa que el cliente cerro todas las sesiones web desde su whatsapp
                session.state = "STARTING";
                session.botStatus = 'notLogged';
                await updateSession(sessionName, session);
                session.client = Sessions.initSession(sessionName, session);
                Sessions.saveClientContext(sessionName, { client: session.client });
                Sessions.setup(sessionName);
            } else if (["CONFLICT", "UNLAUNCHED"].includes(session.state)) {
                Sessions.getClientContext(sessionName);
                session.client.then(client => {
                    client.useHere();
                });
            }
            return session;
        } //start
    
        static async addSession(sessionName) {
            // Guarda la sesion en redis
            let newSession = await saveSession(sessionName);
            // inicia el bot
            let client = Sessions.initSession(sessionName, newSession);
    
            // guarda el contexto del bot
            Sessions.saveClientContext(sessionName, { client });
    
            // configura bot
            Sessions.setup(sessionName);
            return newSession;
        };
    
        static async initSession(sessionName, session) {
            const client = await venom.create(
                sessionName,
                async (base64Qr) => {
                    session.state = "QRCODE";
                    session.qrCode = base64Qr;
                    await updateSession(sessionName, session);
                },
                async (statusFind) => {
                    session.botStatus = statusFind;
                    if (statusFind === 'isLogged' || statusFind === 'qrReadSuccess') {
                        session.state = 'CONNECTED';
                        delete session.qrCode;
                    }
                    await updateSession(sessionName, session);
                }, config.VENOM_CONFIG
            );
            return client;
        }
    
        static async setup(sessionName) {
            let session = Sessions.getClientContext(sessionName)
            if (session) {
                console.log('Setting up..', { session });
                await session.client.then(client => {
                    client.onStateChange(async (state) => {
                        console.log('STATE CHANGE: ', { state });
                        session.state = state;
                        await updateSession(sessionName, session);
                    });
                    client.onMessage((message) => {
                        if (message.body === 'ping') {
                            client.sendText(message.from, 'pong');
                        }
                    });
                    // TODO:  Crear un room para un cliente... y devolver la conexion al socket..
                });
            }
    
        } //setup
    
        static async closeSession(sessionName) {
            const session = await findSession(sessionName);
            if (session) {
                if (session.state != "CLOSED") {
                    if (session.client)
                        await session.client.then(async client => {
                            try {
                                await client.close();
                            } catch (error) {
                                console.log("client.close(): " + error.message);
                            }
                            session.state = "CLOSED";
                            session.client = false;
                        });
                    await deleteSession(sessionName);
                    await Sessions.deleteToken(sessionName);
                    return { ok: true, state: "CLOSED" };
                } else { //close
                    return { ok: true, state: session.state };
                };
            } else {
                return { ok: false, state: "NOTFOUND" };
            }
        } //close
        static deleteToken(sessionName) {
            const tokenPath = path.resolve(__dirname, "..", "..", "tokens", `${sessionName}.data.json`);
            if (fs.existsSync(tokenPath)) {
                return new Promise((resolve, reject) => {
                    fs.unlink(tokenPath, () => {
                        resolve();
                    });
                });
            }
        };
    
        static async getSession(sessionName) {
            let foundSession = await findSession(sessionName)
            return foundSession ? foundSession : false;
        };
    
    
        static async getQrcode(sessionName, toBuffer) {
            let qrCode;
            let session = await findSession(sessionName);
            if (session.botStatus != 'isLogged' && session.qrCode) {
                if (toBuffer) {
                    qrCode = session.qrCode.replace('data:image/png;base64,', '');
                    return Buffer.from(qrCode, 'base64');
                } else {
                    return { ok: true, qrCode: session.qrCode, state: session.state }
                }
            } else {
                return { ok: false, state: session.state };
            }
        }
    
        static saveClientContext(sessionName, clientCtx) {
            Sessions.sessions.set(sessionName, clientCtx);
        }
    
        static getClientContext(sessionName) {
            return Sessions.sessions.get(sessionName);
        }
    
        static async sendText(sessionName, number, text) {
            let session = await findSession(sessionName);
            if (session) {
                let cctx = Sessions.getClientContext(sessionName);
                if (session.state === "CONNECTED") {
                    // Retrieve client promise ctx
                    let client = await cctx.client;
                    client.sendText(number, text);
                    return { ok: true, message: 'Text message sended successfully!' }
                } else {
                    return { ok: false, message: 'Please try to reconnect', state: session.state };
                }
            } else {
                return { ok: false, message: "Session not found..." };
            }
        }; //message
    
        static async getMessages(sessionName, chatId, all) {
            const session = await Sessions.getSession(sessionName);
            if (session) {
                let cctx = Sessions.getClientContext(sessionName);
                session.client = cctx;
                if (session.state === "CONNECTED") {
                    let result = await session.client.then(async client => {
                        if (all) return await client.loadEarlierMessages(chatId);
                        return await client.getAllMessagesInChat(chatId);
                    });
                    console.log('GETTING MESSAGES FROM ', chatId);
                    return { ok: true, data: messageParser(result) };
                }
            } else {
                return { ok: false, message: "Session not found..." }
            }
        };
    }
}



// module.exports = Sessions;
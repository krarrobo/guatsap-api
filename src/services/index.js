module.exports = {
    UserService: require('./user.service'),
    AuthService: require('./auth.service'),
    KeyService: require('./key.service'),
    PhoneNumberService: require('./phoneNumber.service'),
    SocketManager: require('./socket.service'),
    SessionService: require('./session-manager')
}
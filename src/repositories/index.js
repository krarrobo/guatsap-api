module.exports = {
    UserRepository: require('./user.repository'),
    KeyRepository: require('./key.repository'),
    PhoneNumberRepository: require('./phoneNumber.repository'),
    SessionRepository: require('./session.repository'),
}
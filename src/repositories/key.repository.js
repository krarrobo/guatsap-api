const BaseRepository = require("./base.repository");
let _key = null;

class KeyRepository extends BaseRepository {
    constructor({ Key }) {
        super(Key);
        _key = Key;
    }
    async getAllByUserId(userId) {
        return await _key.find({ client: userId });
    }
    async findByApiKey(apiKey) {
        return await _key.findOne({ secretKey: apiKey });
    }
}

module.exports = KeyRepository;
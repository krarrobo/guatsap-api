const BaseRepository = require("./base.repository");
let _phoneNumber = null;

class PhoneNumberRepository extends BaseRepository {
    constructor({ PhoneNumber }) {
        super(PhoneNumber);
        _phoneNumber = PhoneNumber;
    }
    async getAllByUserId(userId) {
        return await _phoneNumber.find({ client: userId });
    }

    async findByPhoneNumber(number) {
        return await _phoneNumber.findOne({ number });
    }
}

module.exports = PhoneNumberRepository;
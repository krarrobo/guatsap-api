const BaseRepository = require("./base.repository");
let _session = null;

class SessionRepository extends BaseRepository {
    constructor({ Session }) {
        super(Session);
        _session = Session;
    }
    async getAllByUserId(userId) {
        return await _session.find({ client: userId });
    }
    async getLastSession(userId, sessionAlias) {
        return await _session.find({ client: userId, alias: sessionAlias }).sort({ createdAt: -1 })[0]
    }
}

module.exports = SessionRepository;
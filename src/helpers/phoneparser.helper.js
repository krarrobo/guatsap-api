module.exports = function(number) {
    if (number.charAt(0) === '0') {
        number = '593'.concat(number.slice(1))
    }
    return number.concat('@c.us');
}
module.exports = function(messageList) {
    return messageList.map(message => {
        const { body, t, sender } = message;
        return {
            sender: {
                name: sender.name,
                number: sender.id.user,
            },
            body,
            timestamp: new Date(t * 1000)
        }
    });
}
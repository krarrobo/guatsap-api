module.exports = {
    User: require('./user.model'),
    PhoneNumber: require('./phoneNumber.model'),
    Key: require('./key.model'),
    Transaction: require('./transaction.model'),
    Payment: require('./payment.model'),
    Session: require('./session.model')
}
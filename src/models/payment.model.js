const mongoose = require("mongoose");
const { Schema } = mongoose;

const PaymentModel = new Schema({
    client: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true,
        autopopulate: true
    },
    total: { type: Number, required: true },
    status: { type: String, enum: ['PENDIENTE', 'PAGADO'], default: 'PENDIENTE' }
}, { timestamps: { createdAt: 'created_at' } });

PaymentModel.plugin(require('mongoose-autopopulate'));
module.exports = mongoose.model('payment', PaymentModel);
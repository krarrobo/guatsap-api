const mongoose = require("mongoose");
const { Schema } = mongoose;

const SessionModel = new Schema({
    client: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true,
    },
    alias: { type: String },
    active: { type: Boolean, default: true },
    endedAt: { type: Date },
}, { timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('session', SessionModel);
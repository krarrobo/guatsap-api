const mongoose = require("mongoose");
const { Schema } = mongoose;

const PhoneNumberSchema = new Schema({
    number: { type: String, required: true, unique: true },
    alias: { type: String, required: true },
    verified: { type: Boolean, required: true, default: false },
    client: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    country: { type: String, required: false },
}, { timestamps: { createdAt: 'created_at' } });

// PhoneNumberSchema.plugin(require("mongoose-autopopulate"));
module.exports = mongoose.model('phonenumber', PhoneNumberSchema);
const mongoose = require("mongoose");
const { Schema } = mongoose;

const KeySchema = new Schema({
    secretKey: { type: String, required: true },
    validThru: { type: Date, required: true },
    client: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    quota: { type: Number, default: 100 },
    successfullTransactions: { type: Number },
    failedTransactions: { type: Number },
    country: { type: String, required: false },
}, { timestamps: { createdAt: 'created_at' } });

// KeySchema.plugin(require("mongoose-autopopulate"));
module.exports = mongoose.model('key', KeySchema);
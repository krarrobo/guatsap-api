const mongoose = require("mongoose");
const { Schema } = mongoose;

const TransactionModel = new Schema({
    phoneNumber: {
        type: Schema.Types.ObjectId,
        ref: 'phonenumber',
        required: true,
    },
    type: { type: String, required: true },
    from: { type: String, required: true },
    to: { type: String, required: true },
    message: { type: String, required: true },
}, { timestamps: { createdAt: 'created_at' } });

module.exports = mongoose.model('transaction', TransactionModel);